#!/bin/bash

rm tailscale-plugin-0.2.0.zip
python tailscale-plugin/build.py
podman build . -t docker-hosted.nodeto.site/rundeck:4.9.0 --build-arg RUNDECK_IMAGE=docker-proxy.nodeto.site/rundeck/rundeck:4.9.0
podman push docker-hosted.nodeto.site/rundeck:4.9.0
