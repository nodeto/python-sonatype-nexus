#!/usr/bin/python3

"""Retrieve the string with environment variables."""

import os
import base64
import bz2
import json
import sys

json_vars = bz2.decompress(base64.b64decode(sys.stdin.read())).decode()
os.environ.update(json.loads(json_vars))
os.execle("/bin/sh", "/bin/sh", "-c", sys.argv[1], os.environ)
