#!/bin/bash
set -e

cd "$RD_PLUGIN_TMPDIR"
yq="$RD_PLUGIN_TMPDIR/yq"

if [ ! -f "$yq" ]; then
    /usr/bin/curl -qL https://github.com/mikefarah/yq/releases/download/v4.30.8/yq_linux_amd64.tar.gz 2> /dev/null \
    | tar xzvf - ./yq_linux_amd64 > /dev/null \
    && mv ./yq_linux_amd64 "$yq"
fi

tailscale status --json | "$yq" -M --output-format json \
'.Peer[]
| select(.Tags != null and .sshHostKeys != null)
| { .HostName: {
    "tags": .Tags | map(sub("^tag:", "")) | join(","),
    "hostname": .DNSName,
    "nodename": .HostName,
    "username": "rundeck",
    "host_keys": .sshHostKeys | join(",")
    }
  }
  as $item ireduce ({}; . * $item)
'
