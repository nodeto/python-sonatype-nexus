import contextlib
import pathlib
import tempfile
from typing import Generator, List, Literal


@contextlib.contextmanager
def key_file(host_keys: str, host_name: str) -> Generator[pathlib.Path, None, None]:
    """Place keys in a temporary file.

    host_keys: Comma-separated list of host keys.
    host_name: Host name.

    yield: Temporary file as Path object.
    """
    with tempfile.TemporaryDirectory() as tmpdir:
        hosts_file = pathlib.Path(tmpdir) / "hosts"
        hosts_file.write_text(
            "\n".join(f"{host_name} {x}" for x in host_keys.split(",")),
            encoding="utf-8",
        )
        yield hosts_file


@contextlib.contextmanager
def openssh_command(
    secure_command: Literal["ssh", "scp"], host_name: str, host_keys: str
) -> Generator[List[str], None, None]:
    contents_dir = pathlib.Path(__file__).parent
    with key_file(host_keys, host_name) as host_keys_file:
        yield (
            f"/usr/bin/{secure_command}",
            "-F",
            contents_dir / "ssh_config",
            "-o",
            f"UserKnownHostsFile={host_keys_file}",
        )
