#!/usr/bin/python3

import os
import json
import sys
import bz2
import base64
import subprocess
import shlex

import command
from copy_getenv import copy_getenv

username = sys.argv[1]
hostname = sys.argv[2]
rd_command = os.environ['RD_EXEC_COMMAND']
host_keys = os.environ['RD_NODE_HOST_KEYS']

rundeck_vars = {k: v for k, v in os.environ.items() if k.startswith("RD_")}
subprocess_input = base64.b64encode(
    bz2.compress(json.dumps(rundeck_vars).encode(), compresslevel=1)
)

with command.openssh_command("ssh", hostname, host_keys) as ssh_command:
    ssh_command_exec = ssh_command + (
        f"{username}@{hostname}",
        ".local/bin/getenv.py",
        shlex.quote(rd_command),
    )

    copy_tried = False
    for _ in range(2):
        try:
            subprocess.run(ssh_command_exec, input=subprocess_input, check=True)
        except subprocess.CalledProcessError as exc:
            if not copy_tried:
                copy_getenv(username, hostname, ssh_command)
                copy_tried = True
                continue
            raise exc
        else:
            break
