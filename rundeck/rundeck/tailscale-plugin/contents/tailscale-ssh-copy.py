#!/usr/bin/python3
import os
import subprocess
import sys

import command

username = sys.argv[1]
hostname = sys.argv[2]
source = sys.argv[3]
destination = sys.argv[4]
host_keys = os.environ['RD_NODE_HOST_KEYS']

with command.openssh_command("scp", hostname, host_keys) as scp_command:
    subprocess.run(
        scp_command + (source, f"{username}@{hostname}:{destination}"), check=True
    )
    print(destination)
