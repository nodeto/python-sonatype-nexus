import pathlib
import subprocess

import command


def copy_getenv(username, hostname, ssh_command):
    scp_command = ("/usr/bin/scp") + ssh_command[1:]
    ssh_command = ssh_command + (f"{username}@{hostname}",)
    current_dir = pathlib.Path(__file__).parent
    subprocess.run(ssh_command + ("/usr/bin/mkdir", "-p", ".local/bin"), check=True)
    scp_command = ssh_command + (
        str(current_dir.joinpath("getenv.py")),
        f"{username}@{hostname}:.local/bin/",
    )
    subprocess.run(
        scp_command,
        check=True,
    )
    subprocess.run(
        ssh_command + ("/usr/bin/chmod", "0700", ".local/bin/getenv.py"), check=True
    )
