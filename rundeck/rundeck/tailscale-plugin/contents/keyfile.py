import contextlib
import pathlib
import tempfile
from typing import Generator, List


if __name__ == "__main__":
    with key_file("a,b,c", "my_host") as my_file:
        print(pathlib.Path(my_file).read_text())
