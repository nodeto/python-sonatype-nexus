import shutil
import pathlib
import tempfile
import subprocess

import yaml

file_path = pathlib.Path(__file__).parent
plugin_data = yaml.safe_load(file_path.joinpath("plugin.yaml").read_text())

with tempfile.TemporaryDirectory() as tmpdir:
    dir_name = f"{file_path.name}-{plugin_data['version']}/"
    zip_name = dir_name[:-1] + ".zip"
    tmp_path = pathlib.Path(tmpdir, dir_name)
    shutil.copytree(f"{file_path}", f"{tmp_path}")
    subprocess.run(["/usr/bin/zip", "-r", zip_name, dir_name], cwd=tmpdir, check=True)
    if file_path.parent.joinpath(zip_name).exists():
        file_path.parent.joinpath(zip_name).unlink()
    shutil.move(pathlib.Path(tmpdir, zip_name), file_path.parent)
