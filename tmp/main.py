#!/usr/bin/env python3.10

"""Check if the dynamic IP at home changes and update it if it does."""

import contextlib
import copy
import itertools
import logging
import os
import pathlib
import subprocess  # nosec
import sys
import time
from typing import Any, Iterator

import lexicon.config
import lexicon.client
import lexicon.exceptions
import requests
import requests.exceptions
import tenacity
import toml
import typer

app = typer.Typer()
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def https_proxy(proxy: str | None) -> Iterator:
    """Use a proxy for https connection and return environment to normal on exit.

    :param proxy: Url of the proxy
    :yield iterator for contextmanager
    """
    if proxy is not None:
        original_proxy = None
        try:
            original_proxy = os.getenv("https_proxy", None)
            os.environ["https_proxy"] = proxy
            yield None
        finally:
            if original_proxy is not None:
                os.environ["https_proxy"] = original_proxy
            else:
                os.environ.pop("https_proxy")
    else:
        yield None


def check_internet(_: Any) -> None:
    """Check if the Internet is up.

    Does not return until Internet is up.  Loads a list of ips to ping, cycles
    all ips until one responds, returns on response.

    Used with tenacity, therefore context is passed in, but unused

    :param _: not used
    """
    ip_list = (
        pathlib.Path(__file__)
        .parent.joinpath("dns.txt")
        .read_text(encoding="utf-8")
        .splitlines()
    )
    ip_iterator = itertools.cycle(ip_list)
    logger.error(
        "Issues reaching DNS and/or IP checker, ensuring the Internet is reachable "
        "before proceeding."
    )
    for ip_to_check in ip_iterator:
        result = subprocess.run(  # nosec
            ["/usr/bin/ping", "-c", "1", ip_to_check], check=True
        )  # nosec pylint: disable=subprocess-run-check
        if result.returncode == 0:
            logger.info("Internet reachable, trying again")
            break
        time.sleep(60)


class Dns:
    """Class for getting or updating DNS content."""

    def __init__(self, lexicon_config: dict, proxy: str | None = None) -> None:
        """Initialize the class.

        :param lexicon_config: Configuration for the lexicon library
        :param proxy: A proxy to use if the proxy holds the whitelisted IP for
            API access, defaults to None
        """
        self.base_config = lexicon_config
        self.proxy = proxy

    def update_by_name(self, name, content, ttl=60, record_type="A"):
        """Update an existing DNS record.

        :param name: The record name
        :param content: The content for the record, such as an IP for an A
            record or name for CNAME record.
        :param ttl: TTL for DNS cacheing, defaults to 60
        :param record_type: The record type such as TXT, A, CNAME, defaults to
            "A"
        """
        config = copy.deepcopy(self.base_config)
        config.update(
            {
                "type": record_type,
                "action": "update",
                "name": name,
                "content": content,
                "ttl": ttl,
            }
        )
        config = lexicon.config.ConfigResolver().with_env().with_dict(config)
        self.execute(config)

    def get_content(self, name, record_type="A") -> list:
        """Retrieve DNS record content.

        :param name: The record name
        :param record_type: The type of record such as "A" or "CNAME",
            defaults to "A"
        :return: A list with the content of all records for the provided name
            and type.
        """
        config = copy.deepcopy(self.base_config)
        config.update({"type": record_type, "action": "list", "name": name})
        config = lexicon.config.ConfigResolver().with_env().with_dict(config)
        return [x["content"] for x in self.execute(config)]

    def execute(self, config) -> list:
        """Execute a DNS operation.

        :param config: A lexicon config object
        """
        with https_proxy(self.proxy):
            result = lexicon.client.Client(config).execute()
        return result


class DynamicIP:
    """Manage a loop to update the DNS record based on the Internet-visible IP."""

    dns: Dns
    name: str
    address: str
    ip_validate_url: str
    session: requests.Session

    def __init__(self, config: dict) -> None:
        """Initialize the class.

        :param config: A dictionary with expected configuration

            {
                "general":
                    {
                        "ip_check_url": "[a url that returns the seen IP in plain text]",
                        "name": "[A record name to be kept up-to-date with the seen IP]",
                        "dns_api_proxy": "[An option proxy in case it is needed"
                        "for whitelisted API access from a server with a static IP.]",
                    },
                "dns": [a dictionary usable by the dns-lexicon library for dns provider access]
            }
        """
        self.ip_check_url = config["general"]["ip_check_url"]
        self.name = config["general"]["name"]
        self.dns = Dns(config["dns"], config["general"].get("dns_api_proxy"))
        self.address = self.dns.get_content(self.name)[0]
        self.session = requests.Session()

    @tenacity.retry(
        wait=tenacity.wait_exponential(multiplier=1, min=15, max=60),
        retry=tenacity.retry_if_exception_type(
            (
                lexicon.exceptions.ProviderNotAvailableError,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectionError,
            )
        ),
        before_sleep=tenacity.before_sleep_log(logger, logging.ERROR),
        after=check_internet,
    )
    def update_loop(self, check_interval=15):
        """Run the update loop.

        :param check_interval: time to wait between IP checks in seconds, defaults to 15
        """
        while True:
            try:
                current_ip = self.session.get(self.ip_check_url, timeout=10).text
            except Exception as exc:
                logger.error(exc)
                raise exc
            if self.address != current_ip:
                try:
                    self.dns.update_by_name(self.name, current_ip)
                    logger.info("DNS updated to %s", current_ip)
                except Exception as exc:
                    logger.error(exc)
                    raise exc
                self.address = current_ip

            time.sleep(check_interval)


@app.command()
def app_loop(
    config_file: pathlib.Path = pathlib.Path(__file__).parent.joinpath("config.toml"),
):
    """Run the app.

    :param config_file: A toml file representing the configuration dictionary
        as required by the DynamicIP class, defaults to
        pathlib.Path(__file__).parent.joinpath("config.toml")
    """
    dynamic_ip = DynamicIP(toml.load(config_file))
    dynamic_ip.update_loop()


if __name__ == "__main__":
    app()
