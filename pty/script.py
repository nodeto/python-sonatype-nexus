import argparse
import os
import pty
import sys
import time
from typing import List, Tuple

import pyte

class ANSIDecoder:
    def __init__(self) -> None:
        self.screen = pyte.Screen(1024, 2)
        self.stream = pyte.ByteStream(self.screen)

    def decode(self, data: List[bytes]):
        """Process ANSI escape codes on a single line at a time.
        
        If ANSI codes move the cursor off the line this could break."""
        result = []
        for line in data:
            self.stream.feed(line)
            result.append(self.screen.display[0].strip())
            self.stream.feed("\r\n")
        return(result)

class BufferWithNewlineTimings:
    def __init__(self) -> None:
        self._buffer: List[Tuple[int, bytes]]

    def get_timing(self):
        return time.monotonic_ns()

    def add_data(self, data):
        self._buffer.append((self.get_timing(), data))

    def as_bytes(self):
        return b"".join([x[1] for x in self._buffer])

    def get_new_lines(self):
        """Get all new, complete, lines"""
        current_bytes = self.as_bytes()
        if current_bytes.endswith("\r\n"):
            return current_bytes.splitlines()
            self._buffer
        else:
            return current_bytes




class TerminalInspector:
    def __init__(self) -> None:
        self.ansi_decoder = ANSIDecoder()
        self.output_buffer = b""
        self.input_buffer = b""

parser = argparse.ArgumentParser()
parser.add_argument('-a', dest='append', action='store_true')
parser.add_argument('-p', dest='use_python', action='store_true')
parser.add_argument('filename', nargs='?', default='typescript')
options = parser.parse_args()

shell = sys.executable if options.use_python else os.environ.get('SHELL', 'sh')
filename = options.filename
mode = 'ab' if options.append else 'wb'

with open(filename, mode) as script, open(f"{filename}.stdin", mode) as script_stdin:
    def read(fd):
        data = os.read(fd, 1024)
        script.write(data)
        return data

    def read_stdin(fd):
        data = os.read(fd, 1024)
        script_stdin.write(data)
        return data

    print('Script started, file is', filename)
    script.write(('Script started on %s\n' % time.asctime()).encode())

    pty.spawn(shell, read, read_stdin)

    script.write(('Script done on %s\n' % time.asctime()).encode())
    print('Script done, file is', filename)
