import pathlib
import subprocess

input = pathlib.Path('typescript').read_bytes()

for line in input.splitlines():
    line = subprocess.check_output(["cat"], input=line)
    print(line)
