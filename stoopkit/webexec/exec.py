import collections
import dataclasses
import os
import pty
import queue
import select
import termios
import threading
from typing import List, MutableMapping, NamedTuple, Optional

import pyte

CHILD = 0


@dataclasses.dataclass
class ExecInfo:
    argv: List[str]
    environ: Optional[MutableMapping]
    cols: int = 80
    lines: int = 24

    def __post_init__(self):
        if self.environ is None:
            self.environ = os.environ.copy()


@dataclasses.dataclass
class ProcessInfo:
    pid: int = -1
    file_d: int = -1


CharFormat: NamedTuple = collections.namedtuple(
    "CharFormat", [x for x in pyte.screens.Char._fields if x != "data"]
)


def get_format_data(char: pyte.screens.Char):
    format_data = char._asdict()
    format_data.pop("data")
    return CharFormat(**format_data)


def format_term_line_as_html(line: pyte.screens.StaticDefaultDict):
    html_line = ""
    default_format = CharFormat(
        fg="default",
        bg="default",
        bold=False,
        italics=False,
        underscore=False,
        strikethrough=False,
        reverse=False,
        blink=False,
    )
    current_format = default_format
    for col in len(line):
        char: pyte.screens.Char = line[col]
        new_format = get_format_data(char)
        if new_format != current_format and current_format != default_format:
            print("</span>", end="")
        if new_format != current_format:
            if new_format != default_format:
                print('<span style="', end="")
                if new_format.fg != 'default':
                    print(f" color: #{new_format.fg}", end="")
                if new_format.bg != "default":
                    print(f" background-color: #{new_format.bg}", end="")
                for style in CharFormat._fields:
                    if getattr(new_format, style):
                        

                print(';">', end="")
            current_span = new_span
        print(char.data, end="")
        return "".join(html_line)


class Term:
    def __init__(self, exec_info: ExecInfo) -> None:
        self.screen = pyte.Screen(exec_info.cols, exec_info.lines)
        self.stream = pyte.Stream(self.screen)
        self.scrollback = queue.Queue()
        self.newline_count = 0
        self.changed = False
        self.lock = threading.Lock()

    def feed(self, data: bytes):
        with self.lock:
            decoded_data = data.decode()
            split_data = decoded_data.split("\n")
            for line in split_data[:-1]:
                # newlines push lines into scrollback
                # I think? this is what all emulators do -- ie screen clears are not saved
                self.newline_count += 1
                if self.newline_count > self.screen.lines:
                    self.scrollback.put(self.screen.buffer[0])
                self.stream.feed(f"{line}\n")
            if split_data[-1]:
                self.stream.feed(f"{line}")
            self.changed = True

    def as_html(self):
        pass


class Execution:
    def __init__(self, argv, environ=None, cols=80, lines=24) -> None:
        self.exec_info = ExecInfo(argv, environ, cols, lines)
        self.recording_thread = threading.Thread(target=self.record, args=(self,))
        self.process = ProcessInfo()

    def record(self):
        while select.select((self.process.file_d,), (), ()):
            try:
                print(os.read(self.process.file_d, 1024))
            except OSError as exc:  # Linux EOF
                if exc.errno == 5:
                    break
                raise exc

    def exec(self):
        """Spawn a process and connect its controlling terminal to a PTY.

        Create a new PTY device and spawn a process with the controlling
        terminal of the child process set to the slave end of the new PTY.

        Args:
        argv: arguments (including executable name) for the child process.

        Returns:
        A pair containing the PID of the child process and the file
        descriptor for the master end of the new PTY device.
        """

        self.exec_info.environ["TERM"] = "linux"
        self.process.pid, self.process.file_d = pty.fork()

        if self.process.pid == CHILD:
            with open("/dev/tty", "rb") as tty_fd:
                termios.tcsetwinsize(
                    tty_fd.fileno(), (self.exec_info.lines, self.exec_info.cols)
                )
            os.execvpe(
                self.exec_info.argv[0], self.exec_info.argv, self.exec_info.environ
            )


if __name__ == "__main__":
    pass
