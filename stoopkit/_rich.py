"""Patch some functions from rich."""

import functools
from typing import Iterable, List, Optional

import rich.ansi
import rich.color
import rich.style
import rich.text
from rich.terminal_theme import DEFAULT_TERMINAL_THEME, TerminalTheme

def decode(self, terminal_text: str) -> Iterable[rich.text.Text]:
    """Split on \r\n and not \r."""
    for line in terminal_text.split("\r\n"):
        yield self.decode_line(line)

rich.ansi.AnsiDecoder.decode = decode

@functools.lru_cache(maxsize=1024)
def get_html_style(self, theme: Optional[TerminalTheme] = None) -> str:
    """Get a CSS style rule.
    
    Patch is to add text decoration color to inline styles only if inline
    styles are present."""
    theme = theme or DEFAULT_TERMINAL_THEME
    css: List[str] = []
    append = css.append

    color = self.color
    bgcolor = self.bgcolor
    if self.reverse:
        color, bgcolor = bgcolor, color
    if self.dim:
        foreground_color = (
            theme.foreground_color if color is None else color.get_truecolor(theme)
        )
        color = rich.color.Color.from_triplet(
            rich.color.blend_rgb(foreground_color, theme.background_color, 0.5)
        )
    if color is not None:
        theme_color = color.get_truecolor(theme)
        append(f"color: {theme_color.hex}")
    if bgcolor is not None:
        theme_color = bgcolor.get_truecolor(theme, foreground=False)
        append(f"background-color: {theme_color.hex}")
    if self.bold:
        append("font-weight: bold")
    if self.italic:
        append("font-style: italic")
    if self.underline:
        append("text-decoration: underline")
    if self.strike:
        append("text-decoration: line-through")
    if self.overline:
        append("text-decoration: overline")
    if any((self.underline, self.strike, self.overline)):
        append(f"text-decoration-color: {theme_color.hex}")
    return "; ".join(css)

rich.style.Style.get_html_style = get_html_style
