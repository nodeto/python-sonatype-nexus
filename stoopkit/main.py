"""Minimal Starlite application."""
import pathlib
import starlite
import starlite.response
import uvicorn

@starlite.get("/", media_type=starlite.MediaType.HTML)
def hello_world() -> str:
    """Handler function that returns a greeting dictionary."""
    return pathlib.Path("basic.html").read_text("utf-8")

@starlite.get("/hx", media_type=starlite.MediaType.HTML)
def hx() -> str:
    return """<tr><td><b>00:00:00</b> Processing... <span style=" color: #f92672;">━━</span><span style=" color: #3a3a3a;">╺━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━</span> <span style=" color: #d338d3;">  5%</span> <span style=" color: #33bbc8;">-:--:--</span></td></tr>
                <tr hx-get="/hx" hx-swap="outerHTML" hx-trigger="every 50ms"></tr>"""

@starlite.get("/css/styles.css")
def css() -> starlite.response.FileResponse:
    return starlite.response.FileResponse("css/styles.css", media_type="text/css")

@starlite.get("/css/ansi2html.css")
def css_ansi() -> starlite.response.FileResponse:
    return starlite.response.FileResponse('css/ansi2html.css', media_type="text/css")

app = starlite.Starlite(route_handlers=[hello_world, hx, css, css_ansi], )

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)    
