"""Used to generate the spec for the pods / containers"""

import re
import subprocess

output = subprocess.check_output(["/usr/bin/podman", "create", "--help"])


# pull never -- since the image should be built to be secure
# remember when pod type don't allow publish

skip_args = [
    "arch",
    "attach",
    "cidfile",
    "conmon_pidfile",
    "disable_content_trust",
    "env",
    "env_file",
    "env_host",
    "log_driver",
    "log_opt",
    "os",
    "platform",
    "pod",
    "pod_id_file",
    "pull",
    "replace",
    "restart",
    "rm",
    "rootfs",
    "sdnotify",
    "stop_signal",
    "systemd",
    "variant",

    ""
]

for line in output.decode().split("Options:")[1].splitlines():
    match = re.search(r"--(\S+)\s(\S+|)", line)
    if match is not None:
        arg, argtype = match.group(1).replace("-", "_"), match.group(2)
        if argtype == "":
            argtype = "BoolArg"
        elif argtype == "float":
            argtype = "FloatArg"
        elif argtype in ["uint", "int"]:
            argtype = "IntArg"
        elif argtype in ["strings", "stringArray"]:
            argtype = "StringListArgs"
        else:
            argtype = "StringArg"

        if arg not in skip_args:
            print(f"{arg}: Optional[{argtype}]")


# {'DEVICE_NAME:WEIGHT', 'strings', 'float', 'uint', 'VARIANT', 'stringArray', 'string', 'ARCH', 'int', 'bool', 'OS', '<number>[<unit>]', 'tmpfs'}
