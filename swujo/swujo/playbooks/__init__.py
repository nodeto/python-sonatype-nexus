import pathlib
import shutil

import ansible_runner
import yaml

from swujo import playbooks


def run_playbook(playbook_name: str, extravars=dict):
    playbook = ansible_runner.run(
        playbook=getattr(playbooks, playbook_name),
        extravars=extravars,
        inventory="localhost,",
        project_dir=str(pathlib.Path(__file__).parent),
    )
    result = playbook.get_fact_cache("localhost")
    shutil.rmtree(playbook.config.private_data_dir)
    return result


def load_playbook(pb_name):
    return yaml.safe_load(
        pathlib.Path(__file__)
        .parent.joinpath(f"{pb_name}.ansible.yml")
        .read_text(encoding="utf-8")
    )


users = load_playbook("users")
