import dataclasses
import pathlib

import pydantic

from swujo.spec import ClaimDirAction, SimpleAction


class Settings(pydantic.BaseModel):
    prune: bool = True
    project_dir: pathlib.Path = pathlib.Path("/etc/opt/nodeto.com/swujo/projects")
    podman_path: pathlib.Path = pathlib.Path("/usr/bin/podman")


@dataclasses.dataclass
class SupportedActions:
    renumber_user: type = SimpleAction
    add_user: type = SimpleAction
    claim_dirs: type= ClaimDirAction
    
    def __str__(self):
        return ", ".join(list(self))

    def __iter__(self):
        for action_name in dataclasses.asdict(self):
            yield action_name



supported_actions = SupportedActions()


if __name__ == "__main__":
    breakpoint()