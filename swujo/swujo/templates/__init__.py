import jinja2

templates = jinja2.Environment(
    loader=jinja2.PackageLoader("swujo", "templates"), autoescape=False
)  # nosec
