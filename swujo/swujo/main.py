import pathlib
import shutil
from importlib.resources import path
from typing import List

import ansible_runner
import tomli
import typer
import yaml

from swujo import playbooks, settings, spec


def restructure_args(project_def):
    """Restructure the args so that the key is set to the name and the value is the value."""
    if "container" in project_def:
        args = project_def["container"]["args"]
    else:
        raise ValueError("No container(or pod?) found.")
    for arg, value in args.items():
        args[arg] = {"name": arg, "value": value}


def get_projects(projects_path: pathlib.Path) -> List[spec.Project]:
    projects = []
    for project_file in projects_path.iterdir():
        if project_file.name.endswith("yml") or project_file.name.endswith("yaml"):
            project_def = yaml.safe_load(project_file.read_text(encoding="utf-8"))
            restructure_args(project_def)
            projects.append(spec.Project(**project_def))
    return projects


def main(
    config_file: pathlib.Path = typer.Option(
        pathlib.Path("/etc/opt/nodeto.com/swujo/settings.toml")
    ),
):
    config = settings.Settings(**tomli.loads(config_file.read_text("utf-8")))
    projects = get_projects(projects_path=config.project_dir)
    for project in projects:
        project.build_containers()

    breakpoint()
    # print(run_playbook("users", {"users": ["swujo-boinc"]}))


def start():
    typer.run(main)


if __name__ == "__main__":
    start()
