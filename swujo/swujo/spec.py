import enum
import pathlib
import shlex
from codeop import CommandCompiler
from multiprocessing.sharedctypes import Value
import subprocess
import tempfile
from typing import Any, Iterable, List, Optional, Sequence

from pydantic import BaseModel  # pylint: disable=no-name-in-module
from pydantic import ValidationError, validator
from typing_extensions import Self

from swujo import playbooks, settings
from swujo.templates import templates


class BuildEnum(str, enum.Enum):
    CONTAINER = "container"
    POD = "pod"


class Action(BaseModel):
    def __new__(self, **action):
        supported_actions_str = f"Supported actions are {settings.supported_actions}"
        if "name" not in action:
            raise ValueError(
                f"Found action with no name! {action}, {supported_actions_str}"
            )
        if action["name"] in settings.supported_actions:
            return getattr(settings.supported_actions, action["name"])(**action)

        raise ValueError(
            f"{action['name']} not a valid action! {supported_actions_str}"
        )

    def __init__(__pydantic_self__, **data: Any) -> None:
        data["params"] = data.copy()
        super().__init__(**data)


# Actions for securing template
class SimpleAction(BaseModel):
    name: str


class ClaimDirAction(SimpleAction):
    dirs: List[str]


# Args for container create / pod create
class Arg(BaseModel):
    name: str

    def get_arg(self):
        return [f"--{self.name.replace('_','-')}", str(self.value)]


class BoolArg(Arg):
    value: bool

    def get_arg(self):
        return [f"--{self.name.replace('_','-')}={str(self.value).lower()}"]


class FloatArg(Arg):
    value: float


class IntArg(Arg):
    value: int


class StringArg(Arg):
    value: str


class StringListArgs(Arg):
    value: List[str]

    def get_arg(self):
        args = []
        for value in self.value:
            args.extend(StringArg(name=self.name, value=value).get_arg())
        return args


class SecureSpec(BaseModel):
    source_image: str
    user: str
    actions: List[Action]


class CommandArgs(BaseModel):
    def get_command_args(self):
        """Get the args needed for the create command."""
        args = []
        for arg in self.dict().values():
            if arg is not None:
                arg_obj = getattr(self, arg["name"])
                args.append(f"{shlex.join(arg_obj.get_arg())}")
        return args


class ContainerArgs(CommandArgs):
    add_host: Optional[StringListArgs]
    annotation: Optional[StringListArgs]
    authfile: Optional[StringArg]
    blkio_weight: Optional[StringArg]
    blkio_weight_device: Optional[StringArg]
    cap_add: Optional[StringListArgs]
    cap_drop: Optional[StringListArgs]
    cgroup_conf: Optional[StringListArgs]
    cgroup_parent: Optional[StringArg]
    cgroupns: Optional[StringArg]
    cgroups: Optional[StringArg]
    chrootdirs: Optional[StringListArgs]
    cpu_period: Optional[IntArg]
    cpu_quota: Optional[IntArg]
    cpu_rt_period: Optional[IntArg]
    cpu_rt_runtime: Optional[IntArg]
    cpu_shares: Optional[IntArg]
    cpus: Optional[FloatArg]
    cpuset_cpus: Optional[StringArg]
    cpuset_mems: Optional[StringArg]
    device: Optional[StringListArgs]
    device_cgroup_rule: Optional[StringListArgs]
    device_read_bps: Optional[StringListArgs]
    device_read_iops: Optional[StringListArgs]
    device_write_bps: Optional[StringListArgs]
    device_write_iops: Optional[StringListArgs]
    dns: Optional[StringListArgs]
    dns_opt: Optional[StringListArgs]
    dns_search: Optional[StringListArgs]
    entrypoint: Optional[StringArg]
    expose: Optional[StringListArgs]
    gidmap: Optional[StringListArgs]
    group_add: Optional[StringListArgs]
    health_cmd: Optional[StringArg]
    health_interval: Optional[StringArg]
    health_retries: Optional[IntArg]
    health_start_period: Optional[StringArg]
    health_timeout: Optional[StringArg]
    hostname: Optional[StringArg]
    hostuser: Optional[StringListArgs]
    http_proxy: Optional[BoolArg]
    image_volume: Optional[StringArg]
    init: Optional[BoolArg]
    init_ctr: Optional[StringArg]
    init_path: Optional[StringArg]
    interactive: Optional[BoolArg]
    ip: Optional[StringArg]
    ip6: Optional[StringArg]
    ipc: Optional[StringArg]
    label: Optional[StringListArgs]
    label_file: Optional[StringListArgs]
    mac_address: Optional[StringArg]
    memory: Optional[StringArg]
    memory_reservation: Optional[StringArg]
    memory_swap: Optional[StringArg]
    memory_swappiness: Optional[IntArg]
    name: Optional[StringArg]
    network: Optional[StringListArgs]
    network_alias: Optional[StringListArgs]
    no_healthcheck: Optional[BoolArg]
    no_hosts: Optional[BoolArg]
    oom_kill_disable: Optional[BoolArg]
    oom_score_adj: Optional[IntArg]
    passwd_entry: Optional[StringArg]
    personality: Optional[StringArg]
    pid: Optional[StringArg]
    pidfile: Optional[StringArg]
    pids_limit: Optional[IntArg]
    privileged: Optional[BoolArg]
    publish: Optional[StringListArgs]
    publish_all: Optional[BoolArg]
    quiet: Optional[BoolArg]
    read_only: Optional[BoolArg]
    read_only_tmpfs: Optional[BoolArg]
    requires: Optional[StringListArgs]
    seccomp_policy: Optional[StringArg]
    secret: Optional[StringListArgs]
    security_opt: Optional[StringListArgs]
    shm_size: Optional[StringArg]
    stop_timeout: Optional[IntArg]
    subgidname: Optional[StringArg]
    subuidname: Optional[StringArg]
    sysctl: Optional[StringListArgs]
    timeout: Optional[IntArg]
    tls_verify: Optional[BoolArg]
    tmpfs: Optional[StringArg]
    tty: Optional[BoolArg]
    tz: Optional[StringArg]
    uidmap: Optional[StringListArgs]
    ulimit: Optional[StringListArgs]
    umask: Optional[StringArg]
    unsetenv: Optional[StringListArgs]
    unsetenv_all: Optional[BoolArg]
    user: Optional[StringArg]
    userns: Optional[StringArg]
    uts: Optional[StringArg]
    volumes_from: Optional[StringListArgs]
    workdir: Optional[StringArg]


class PodmanBase(BaseModel):
    name: str
    args: CommandArgs


class Container(PodmanBase):
    args: ContainerArgs
    secure: SecureSpec
    pod: Optional[str] = None
    env: Optional[dict] = None
    bind_mounts: Optional[dict] = None

    def get_podman_args(self):
        # add the env to the command line
        args = []
        for key in self.get_env():
            args.append(f"{shlex.join(['--env', key])}")
        args.extend(self.args.get_command_args())
        return args

    def get_env(self):
        if self.env is None:
            return {}
        return self.env

    def build_container(self, user_info):
        template = templates.get_template("Containerfile.j2")
        with tempfile.TemporaryDirectory() as tmp_dir:

            rendered_template = template.render(
                user=user_info[self.secure.user],
                actions=self.secure.actions,
                source_image=self.secure.source_image,
            )
            pathlib.Path(tmp_dir, "Containerfile").write_text(
                rendered_template, encoding="utf-8"
            )
            subprocess.run(
                ["podman", "build", tmp_dir, "--tag", f"localhost/swujo-{self.name}-image"],
                check=True,
            )


class Pod(BaseModel):
    containers: List[Container]


class Project(BaseModel):
    type: BuildEnum
    name: str
    version: Optional[str]
    pod: Optional[Pod]
    container: Optional[Container]

    @validator("pod")
    def type_must_be_pod(cls, value, values, **kwargs):
        if values["type"] != "pod":
            raise ValidationError("Pod type specified but no pod found")
        return value

    @validator("container")
    def type_must_be_container(cls, value, values, **kwargs):
        if values["type"] != "container":
            raise ValidationError("Container type specified but no container found")
        return value

    def build_containers(self):
        if self.container:
            containers = [self.container]
        else:
            containers = self.pod.containers

        users = set()
        for container in containers:
            users.add(container.secure.user)

        user_info = playbooks.run_playbook("users", {"users": list(users)})["user_info"]
        for container in containers:
            container.build_container(user_info)


if __name__ == "__main__":
    breakpoint()
