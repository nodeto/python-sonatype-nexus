#!/bin/sh

if [ $# -eq 1 ]; then
    curl -f -u ${YUM_USER}:${YUM_PASSWORD} --upload-file $1 https://nexus.nodeto.site/repository/yum/
else
    echo "Usage: ./upload.sh filename"
fi
