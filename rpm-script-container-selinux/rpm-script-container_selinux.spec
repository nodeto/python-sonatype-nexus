# vim: sw=4:ts=4:et

%define selinux_policyver 3.14.3-95

Name:   rpm-script-container_selinux
Version:	1.0
Release:	3%{?dist}
Summary:	SELinux policy module for rpm-script-container

Group:	System Environment/Base		
License:	GPLv2+	
# This is an example. You will need to change it.
URL:		http://HOSTNAME
Source0:	rpm-script-container.pp
Source1:	rpm-script-container.if

Requires: policycoreutils, libselinux-utils, container-selinux
Requires(post): selinux-policy-base >= %{selinux_policyver}, policycoreutils, container-selinux
Requires(postun): policycoreutils
BuildArch: noarch

%description
This package installs and sets up the  SELinux policy security module for rpm-script-container.

%install
install -d %{buildroot}%{_datadir}/selinux/packages
install -m 644 %{SOURCE0} %{buildroot}%{_datadir}/selinux/packages
install -d %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/selinux/devel/include/contrib/
install -d %{buildroot}%{_mandir}/man8/

install -d %{buildroot}/etc/selinux/targeted/contexts/users/

%post
semodule -n -i %{_datadir}/selinux/packages/rpm-script-container.pp
if /usr/sbin/selinuxenabled ; then
    /usr/sbin/load_policy
    
fi;
exit 0

%postun
if [ $1 -eq 0 ]; then
    semodule -n -r rpm-script-container 2>/dev/null 1>&2
    if /usr/sbin/selinuxenabled ; then
       /usr/sbin/load_policy
       

    fi;
fi;
exit 0

%files
%attr(0600,root,root) %{_datadir}/selinux/packages/rpm-script-container.pp
%{_datadir}/selinux/devel/include/contrib/rpm-script-container.if



%changelog
* Mon Sep 19 2022 YOUR NAME <YOUR@EMAILADDRESS> 1.0-1
- Initial version

