#!/usr/bin/python3
"""Cleans local directories and starts a new deb package build."""
import pathlib
import shutil
import subprocess  # nosec
import yaml

config = yaml.safe_load(pathlib.Path("package.yml").read_text("UTF-8"))

build_dir = pathlib.Path(
    f"{config['provider']}-{config['app_slug']}-{config['version']}"
)
cookiecutter_dir = pathlib.Path("cookiecutter")

for dir_path in (build_dir, cookiecutter_dir):
    if dir_path.is_dir():
        shutil.rmtree(dir_path)

subprocess.run(  # nosec
    [
        "/usr/bin/git",
        "clone",
        "https://gitlab.com/nodeto/compose-debian-package-builder.git",
        str(cookiecutter_dir),
    ],
    check=True,
)

subprocess.run(  # nosec
    [
        cookiecutter_dir.joinpath("build.py"),
        "package.yml",
        cookiecutter_dir.joinpath("cookiecutter"),
    ],
    check=True,
)
