oauth2-proxy \
--provider oidc \
--provider-display-name Rundeck \
--client-secret XrAQ7Ox92lb43uquPMHCcQjF1VHlBeWcK3tHYEakTQE \
--client-id 5935c83a-d4c6-4b53-9e83-3742c27efffe \
--redirect-url http://127.0.0.1:4180/oauth2/callback \
--oidc-issuer-url http://localhost:9011/ \
--cookie-secure=false \
--cookie-secret=W3DMY35MXhhK2Z5_5cJk7kXUf_MQ0ZA2508jy2G78MU \
--email-domain "*" \
--allowed-group admin \
--show-debug-on-error \
--oidc-groups-claim roles \
--skip-oidc-discovery=true
--login-url http://127.0.0.1:9011/oauth2/authorize
--redeem-url=https://<mynamespace>.b2clogin.com/<mynamespace>.onmicrosoft.com/
      b2c_1a_signin/oauth2/v2.0/token
--oidc-jwks-url=https://<mynamespace>.b2clogin.com/<mynamespace>.onmicrosoft.com/
      b2c_1a_signin/discovery/v2.0/keys