from typing import Dict
import uvicorn
from fastapi import FastAPI, Header
import lexicon.client
import lexicon.config
from pydantic import BaseModel


app = FastAPI()


class DnsRequest(BaseModel):
    provider: str
    # provider_config: Dict[str, str]
    # content: str | None = None
    # ttl: int = 120
    # record_type: str = "A"


class DnsResponse(BaseModel):
    content: str
    ttl: str


@app.put("/dns/{domain}/{dns_name}/", response_model=DnsResponse)
async def update_dns(domain: str, dns_name: str, dns_request: DnsRequest):
    breakpoint()
    config = {
        "action": "update",
        "name": dns_name,
        "domain": domain,
        "provider_name": dns_request.provider,
        dns_request.provider: dns_request.provider_config,
        "content": dns_request.content,
        "ttl": dns_request.content,
    }
    breakpoint()
    lexicon_config = lexicon.config.ConfigResolver().with_dict(config)
    result = lexicon.client.Client(lexicon_config).execute()
    breakpoint()


@app.get("/dns/{domain}/{dns_name}/", response_model=DnsResponse)
async def get_dns_content(
    domain: str,
    dns_name: str,
    provider_name: str,
    provider_auth_token: str,
    type: str | None = "A",
):
    breakpoint()
    return {"message": "Hello World"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
