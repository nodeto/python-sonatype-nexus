import dataclasses
import pathlib
import pprint
import shutil
import subprocess
import tempfile
from typing import Any, List

import jinja2
import tomli
import typer


app = typer.Typer()


@dataclasses.dataclass(kw_only=True)
class PackageBase:
    # meta
    app_dir: pathlib.Path
    organization: str
    name: str
    human_name: str
    description: str
    slug: str | None = None
    package_revision: int = 1
    build_image: str
    root_daemon: bool = False

    # yum
    yum_repo_url: str = "https://nexus.nodeto.site/repository/nodeto-el/"
    yum_user: str = "admin"
    yum_password: str

    templates_subdir: str

    # requirements
    requires: List[str] | None = None
    build_requires: List[str] | None = None
    requires_pre: List[str] | None = None
    requires_preun: List[str] | None = None
    requires_post: List[str] | None = None
    requires_postun: List[str] | None = None

    jinja2_env: jinja2.Environment = dataclasses.field(init=False)

    etcdir: str = dataclasses.field(init=False)
    vardir: str = dataclasses.field(init=False)

    def __post_init__(self):
        if self.slug is None:
            self.slug = self.name.replace("_", "-").replace(" ", "-").lower()
        template_path = pathlib.Path(__file__).parent.joinpath("templates")
        self.jinja2_env = jinja2.Environment(  # nosec (no autoescape not html)
            loader=jinja2.FileSystemLoader(
                [
                    str(template_path.joinpath(self.templates_subdir)),
                    str(template_path.joinpath("shared")),
                ],
                encoding="utf-8",
            ),
        )
        self.etcdir = f"/etc/opt/{self.organization}/{self.name}"
        self.vardir = f"/var/opt/{self.organization}/{self.name}"

        for requires_list in [
            "requires",
            "build_requires",
            "requires_pre",
            "requires_post",
            "requires_preun",
            "requires_postun",
        ]:
            if getattr(self, requires_list) is None:
                setattr(self, requires_list, [])

    def template_appfiles(self, tmp_path: pathlib.Path):
        for jinja_file in tmp_path.rglob("*.j2"):
            jinja_template: jinja2.Template = jinja2.Template(jinja_file.read_text(encoding="utf-8"))
            out_file_path = jinja_file.parent.joinpath(jinja_file.name.replace(".j2", ""))
            out_file_path.write_text(jinja_template.render(**self.__dict__), encoding="utf-8")
            jinja_file.unlink()

    def build(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            tmp_path = pathlib.Path(tmpdir)
            shutil.copytree(str(self.app_dir), str(tmp_path), dirs_exist_ok=True)
            self.template_appfiles(tmp_path)
            for template_name in self.jinja2_env.loader.list_templates():
                template = self.jinja2_env.get_template(template_name)
                rendered_template = template.render(**self.__dict__)
                tmp_path.joinpath(pathlib.Path(template_name).stem).write_text(
                    rendered_template, encoding="utf-8"
                )
            subprocess.run(["/usr/bin/podman", "build", str(tmp_path)], check=True)


@dataclasses.dataclass(kw_only=True)
class PythonVenv(PackageBase):
    pypackage_name: str | None = None
    pypackage_version: str

    # install
    config_file: str
    start_bin: str
    config_option: str
    sys_python_package: str
    sys_python_path: str
    sys_config_dir: str = "/etc"

    templates_subdir: str = "python_venv"

    def __post_init__(self):
        super().__post_init__()
        if self.pypackage_name is None:
            self.pypackage_name = self.name.replace("-", "_")

        self.requires.extend([self.sys_python_package, "systemd"])
        self.requires_pre.extend(["shadow-utils"])
        self.build_requires.extend(
            [self.sys_python_package, "systemd-rpm-macros", "make"]
        )


@dataclasses.dataclass(kw_only=True)
class Container(PackageBase):
    package_name: str | None = None
    app_version: str
    templates_subdir: str = "container"

    def __post_init__(self):
        super().__post_init__()
        if self.package_name is None:
            self.package_name = self.name
        self.requires.extend(
            ["systemd", "systemd-rpm-macros", "nodeto.com-podman-compose >= 1.0.4"]
        )
        self.requires_post.extend(
            [
                "shadow-utils",
                "python3",
                "grep",
                "policycoreutils",
                "policycoreutils-python-utils",
                "container-selinux",
            ]
        )
        self.requires_postun.extend(
            ["python3", "podman", "util-linux"]
        )

@app.command()
def build(
    app_dir: pathlib.Path,
    yum_password=typer.Option(..., envvar="PACKAGE_BUILDER_YUM_PASSWORD"),
    build_type=typer.Option(..., envvar="PACKAGE_BUILDER_BUILD_TYPE"),
):
    builder_class: Any
    if build_type == "python_venv":
        pyproject_data = tomli.loads(
            app_dir.joinpath("pyproject.toml").read_text(encoding="utf-8")
        )
        pprint.pprint(pyproject_data)
        build_vars = pyproject_data["tool"]["nodeto-package-builder"]
        poetry_vars = pyproject_data["tool"]["poetry"]
        build_vars["name"] = poetry_vars["name"]
        build_vars["description"] = poetry_vars["description"]
        build_vars["pypackage_version"] = poetry_vars["version"]
        builder_class = PythonVenv
    elif build_type == "container":
        build_vars = tomli.loads(
            app_dir.joinpath("package-builder.toml").read_text(encoding="utf-8")
        )
        builder_class = Container

    build_vars["app_dir"] = app_dir
    build_vars["yum_password"] = yum_password
    builder = builder_class(**build_vars)
    builder.build()
