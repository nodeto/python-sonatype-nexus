from collections import defaultdict
import json
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile

import requests

import operation_renames

SWAGGER_SOURCE = "https://nexus-2.nodeto.site/service/rest/swagger.json"


def get_api_init_lines(api_generated_lines: str) -> list:
    api_import_lines = [
        x
        for x in api_generated_lines.splitlines()
        if x.startswith("from nexus_generated")
    ]

    api_init_info = [
        ("_".join(x.split()[1].split(".")[-1].split("_")[:-1]), x.split()[-1])
        for x in api_import_lines
    ]
    return [
        f"self.{x[0]} = nexus_generated.{x[1]}(api_client=self.api_client)"
        for x in api_init_info
    ]


operation_ids = defaultdict(set)
swagger_data = json.loads(requests.get(SWAGGER_SOURCE).text)
renames_dict = dict(operation_renames.op_list)
for path, path_value in swagger_data["paths"].items():
    for operation, value in path_value.items():
        # a key error here means we need to run create_path_list.py
        # but first we have to modify it so it only adgs the missing paths to the list
        operation_id = renames_dict[(path, operation)]
        for tag in value["tags"]:
            if operation_id in operation_ids[tag]:
                raise Exception(
                    f"Duplicate operation id: {operation} {operation_id} in {path}, tag {tag}"
                )
            operation_ids[tag].add(operation_id)
        value["operationId"] = operation_id
        print(path, value["operationId"])

shutil.rmtree("nexus_generated", ignore_errors=True)
with tempfile.TemporaryDirectory() as tmpdir:
    swagger_base_command = (
        "/usr/bin/podman",
        "run",
        "-v",
        f"{tmpdir}:/tmp",
        "--rm",
        "-it",
        "docker-proxy.nodeto.site/swaggerapi/swagger-codegen-cli-v3:3.0.40",
    )
    swag_gen_cmd = swagger_base_command + (
        "generate",
        "-i",
        "/tmp/swagger.json",
        "-l",
        "python",
        "-o",
        "/tmp/code",
        "-c",
        "/tmp/config.json",
    )

    tmp_path = pathlib.Path(tmpdir)
    tmp_path.joinpath("swagger.json").write_text(json.dumps(swagger_data))
    code_dir = tmp_path.joinpath("code")
    code_dir.mkdir()
    shutil.copy("config.json", tmp_path)
    subprocess.run(swag_gen_cmd, check=True)
    subprocess.run((sys.executable, "setup.py", "build"), cwd=code_dir, check=True)
    shutil.copytree(
        code_dir.joinpath("build/lib/nexus_generated"),
        pathlib.Path(os.getcwd(), "nexus_generated/"),
    )
    api_init = pathlib.Path("nexus_generated/api/__init__.py").read_text()
    api_init_lines = get_api_init_lines(api_init)

    main_file = pathlib.Path("nexus/main.py")
    main_file_text = main_file.read_text()
    pathlib.Path("nexus_main.py.backup").write_text(main_file_text)
    new_main_file = []
    in_generated_section = False
    for line in main_file_text.splitlines():
        if in_generated_section:
            if line == "        # END GENERATED SECTION":
                in_generated_section = False
                new_main_file.append(line)
        else:
            new_main_file.append(line)

        if line == "        # GENERATED CODE DO NOT EDIT":
            for line in api_init_lines:
                new_main_file.append(" " * 8 + line)
            in_generated_section = True

    main_file.write_text("\n".join(new_main_file))
    subprocess.run(("black", "nexus_generated/"), check=True)
