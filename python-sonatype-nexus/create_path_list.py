import json
import pprint

import requests

from generate import SWAGGER_SOURCE

swagger_data = json.loads(requests.get(SWAGGER_SOURCE).text)
path_list = []
for path, path_value in swagger_data["paths"].items():
    for operation, value in path_value.items():
        operation_id = value["operationId"]
        path_parts = path.split("/")
        if len(path_parts) > 4 and path_parts[4] in ["hosted", "proxy", "group"]:
            operation_id = operation_id.split("_")[0].replace(
                "Repository",
                path_parts[3].capitalize() + path_parts[4].capitalize() + "Repository",
            )
        path_list.append(((path, operation), operation_id))
print("op_list = ", end="")
pprint.pprint(sorted(path_list))
