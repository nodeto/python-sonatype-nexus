import nexus_generated


class ApiClient(nexus_generated.ApiClient):
    def call_api(
        self,
        resource_path,
        method,
        path_params=None,
        query_params=None,
        header_params=None,
        body=None,
        post_params=None,
        files=None,
        response_type=None,
        auth_settings=None,
        async_req=None,
        _return_http_data_only=None,
        collection_formats=None,
        _preload_content=True,
        _request_timeout=None,
    ):
        return_data = super().call_api(
            resource_path,
            method,
            path_params,
            query_params,
            header_params,
            body,
            post_params,
            files,
            response_type,
            auth_settings,
            async_req,
            _return_http_data_only,
            collection_formats,
            _preload_content,
            _request_timeout,
        )
        if return_data.continuation_token:
            return_items = return_data.items
            continuation_token = return_data.continuation_token
            while continuation_token is not None:
                query_dict = dict(query_params)
                query_dict["continuationToken"] = continuation_token
                query_params = [(k, v) for k, v in query_dict.items()]
                result = super().call_api(
                    resource_path,
                    method,
                    path_params,
                    query_params,
                    header_params,
                    body,
                    post_params,
                    files,
                    response_type,
                    auth_settings,
                    async_req,
                    _return_http_data_only,
                    collection_formats,
                    _preload_content,
                    _request_timeout,
                )
                return_items.extend(result.items)
                continuation_token = result.continuation_token
            return return_items
        return return_data


class Nexus:
    def __init__(self, host, username, password):
        config = nexus_generated.Configuration()
        config.host = f"https://{host}/service/rest"
        config.username = username
        config.password = password

        self.api_client = ApiClient(
            configuration=config,
            header_name="Authorization",
            header_value=config.get_basic_auth_token(),
        )

        # GENERATED CODE DO NOT EDIT
        self.assets = nexus_generated.AssetsApi(api_client=self.api_client)
        self.azure_blob_store = nexus_generated.AzureBlobStoreApi(
            api_client=self.api_client
        )
        self.blob_store = nexus_generated.BlobStoreApi(api_client=self.api_client)
        self.components = nexus_generated.ComponentsApi(api_client=self.api_client)
        self.content_selectors = nexus_generated.ContentSelectorsApi(
            api_client=self.api_client
        )
        self.email = nexus_generated.EmailApi(api_client=self.api_client)
        self.formats = nexus_generated.FormatsApi(api_client=self.api_client)
        self.lifecycle = nexus_generated.LifecycleApi(api_client=self.api_client)
        self.manage_iq_server_configuration = (
            nexus_generated.ManageIQServerConfigurationApi(api_client=self.api_client)
        )
        self.product_licensing = nexus_generated.ProductLicensingApi(
            api_client=self.api_client
        )
        self.read_only = nexus_generated.ReadOnlyApi(api_client=self.api_client)
        self.repository_management = nexus_generated.RepositoryManagementApi(
            api_client=self.api_client
        )
        self.routing_rules = nexus_generated.RoutingRulesApi(api_client=self.api_client)
        self.script = nexus_generated.ScriptApi(api_client=self.api_client)
        self.search = nexus_generated.SearchApi(api_client=self.api_client)
        self.security_management_anonymous_access = (
            nexus_generated.SecurityManagementAnonymousAccessApi(
                api_client=self.api_client
            )
        )
        self.security_certificates = nexus_generated.SecurityCertificatesApi(
            api_client=self.api_client
        )
        self.security_management = nexus_generated.SecurityManagementApi(
            api_client=self.api_client
        )
        self.security_management_ldap = nexus_generated.SecurityManagementLDAPApi(
            api_client=self.api_client
        )
        self.security_management_privileges = (
            nexus_generated.SecurityManagementPrivilegesApi(api_client=self.api_client)
        )
        self.security_management_realms = nexus_generated.SecurityManagementRealmsApi(
            api_client=self.api_client
        )
        self.security_management_roles = nexus_generated.SecurityManagementRolesApi(
            api_client=self.api_client
        )
        self.security_management_users = nexus_generated.SecurityManagementUsersApi(
            api_client=self.api_client
        )
        self.status = nexus_generated.StatusApi(api_client=self.api_client)
        self.support = nexus_generated.SupportApi(api_client=self.api_client)
        self.tasks = nexus_generated.TasksApi(api_client=self.api_client)
        # END GENERATED SECTION
