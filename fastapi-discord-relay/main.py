from fastapi import FastAPI
from fastapi.responses import JSONResponse, RedirectResponse

from authlib.integrations.starlette_client import OAuth
from httpx import AsyncClient
from starlette.requests import Request
from starlette.middleware.sessions import SessionMiddleware
import uvicorn

import arrow

app = FastAPI()
app.add_middleware(
    SessionMiddleware, secret_key="aYq7bHHz_NXlrUb8VS8OYX37gFN3h0TzG6vtk5qSjA"
)

oauth_client = OAuth()
http_client = AsyncClient()

DISCORD_URL = "https://discord.com"
DISCORD_API_URL = f"{DISCORD_URL}/api/v8"
DISCORD_OAUTH_URL = f"{DISCORD_URL}/api/oauth2"
DISCORD_TOKEN_URL = f"{DISCORD_OAUTH_URL}/token"
DISCORD_OAUTH_AUTHENTICATION_URL = f"{DISCORD_OAUTH_URL}/authorize"

oauth_client.register(
    name="discord",
    client_id="984267736437899276",
    client_secret="NNLz8sZiaG1Gv3Np4Tggv-6vavX9SiYx",
    access_token_url=DISCORD_TOKEN_URL,
    access_token_params=None,
    authorize_url=DISCORD_OAUTH_AUTHENTICATION_URL,
    authorize_params=None,
    api_base_url=DISCORD_API_URL,
    client_kwargs={"scope": "identify email guilds"},
)


@app.get("/")
async def root(request: Request):
    if (
        request.session.get("token", {"expires_at": 0})["expires_at"]
        < arrow.utcnow().timestamp()
    ):
        return RedirectResponse(request.url_for("login"))
    return RedirectResponse(request.url_for("data"))


@app.get("/data", response_model=dict)
async def data(request: Request):
    headers = {"authorization": dict(request.headers)["authorization"]}
    
    response = await http_client.get(
        f"{DISCORD_API_URL}/users/@me",
        headers=headers,
    )
    user_data = response.json()
    response = await http_client.get(
        f"{DISCORD_API_URL}/users/@me/guilds",
        headers=headers,
    )
    guilds = response.json()
    groups = []
    for guild in guilds:
        groups.append(guild["id"])
    groups = ",".join(groups)
    user_data["groups"] = groups
    user_data["username"] = "john"
    return JSONResponse(user_data)


@app.get("/login/discord")
async def login(request: Request):
    redirect_uri = request.url_for("callback")
    redirect = await oauth_client.discord.authorize_redirect(request, redirect_uri)
    return redirect


@app.get("/callback")
async def callback(request: Request):
    """Gets the user data from an access_token"""
    request.session["token"] = await oauth_client.discord.authorize_access_token(
        request
    )
    request.session["discord_headers"] = {
        "Authorization": f"Bearer {request.session['token']['access_token']}"
    }
    # {'access_token': '0TZ3rLPbPqf3nUeFZyAYO4VFZluPn8', 'expires_in': 604800, 'refresh_token': 'zc9U9zDNER7uPD53TB163cHntiHZ2z', 'scope': 'guilds identify email', 'token_type': 'Bearer', 'expires_at': 1655506490}
    #
    return RedirectResponse(request.url_for("data"))


# @app.exception_handler(Unauthorized)
# async def unauthorized_error_handler(_, __):
#     return JSONResponse({"error": "Unauthorized"}, status_code=401)


# @app.exception_handler(RateLimited)
# async def rate_limit_error_handler(_, e: RateLimited):
#     return JSONResponse(
#         {"error": "RateLimited", "retry": e.retry_after, "message": e.message},
#         status_code=429,
#     )


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
