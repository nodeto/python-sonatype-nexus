import json

from fastapi import FastAPI
from fastapi.responses import JSONResponse
from httpx import AsyncClient
from starlette.requests import Request

app = FastAPI()
http_client = AsyncClient()

DISCORD_URL = "https://discord.com"
DISCORD_API_URL = f"{DISCORD_URL}/api/v8"
GITLAB_USERINFO = "https://gitlab.com/oauth/userinfo"


@app.get("/gitlab")
async def gitlab(request: Request):
    headers = {"authorization": dict(request.headers)["authorization"]}

    response = await http_client.get(
        f"{GITLAB_USERINFO}",
        headers=headers,
    )
    user_data = response.json()
    user_data["given_name"], user_data["family_name"] = user_data["name"].split()[:2]
    user_data["groups"] = ",".join(user_data["groups"])

    return JSONResponse(user_data)


@app.get("/discord", response_model=dict)
async def data(request: Request):
    headers = {"authorization": dict(request.headers)["authorization"]}
    response = await http_client.get(
        f"{DISCORD_API_URL}/users/@me",
        headers=headers,
    )
    user_data = response.json()
    response = await http_client.get(
        f"{DISCORD_API_URL}/users/@me/guilds",
        headers=headers,
    )
    guilds = response.json()
    groups = []
    for guild in guilds:
        groups.append(guild["id"])
    groups = ",".join(groups)
    user_data["groups"] = groups
    return JSONResponse(user_data)
