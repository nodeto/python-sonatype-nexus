import json

import flask

app = flask.Flask(__name__)


@app.route("/")
def print_headers():
    response = flask.Response(
        json.dumps(dict(flask.request.headers), indent=2),
        content_type="application/json",
    )
    return response


@app.route("/.well-known/openid-configuration")
def return_oauth():
    config_dict = {
        "issuer": "http://header_report:5000/",
        "authorization_endpoint": "https://discord.com/api/oauth2/authorize",
        "token_endpoint": "https://discord.com/api/oauth2/token",
        "userinfo_endpoint": "https://discord.com/api/users/@me",
        "grant_types_supported": ["authorization_code,refresh_token"],
        "scopes_supported": ["identity", "email", "guilds"],
    }
    return flask.Response(json.dumps(config_dict), content_type="application/json")
